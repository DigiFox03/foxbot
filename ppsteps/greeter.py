from foxcore.ppstep import PPStep


class Greeter(PPStep):
    def __init__(self, time_limit):
        self.time_limit = time_limit

    def run(self, data, rel_user_act=None, rel_memb_act=None, **_):
        if rel_memb_act is not None and not data["member"]["to_greet"]:
            data["member"]["to_greet"] = rel_memb_act > self.time_limit
        elif rel_memb_act is not None and not data["user"]["to_greet"]:
            data["user"]["to_greet"] = rel_user_act > self.time_limit
        return {"data": data}
