import re
from foxcore.feature import Feature

CHECK = re.compile(r"my name(?:'s| is) (.*)", re.IGNORECASE)
TEST_UNVALID = re.compile(r"[^-a-zA-Z']")
TOO_LONG = (
    "That's a long name, do your friends call you by that name?\n"
    "`Please use a shorter name.`"
    )
TOO_SHORT = (
    "Wow only one letter?\n"
    "`Please use a longer name.`"
)
NO_FULL = (
    "Ok, that's your full name. How do your friends call you?\n"
    "`Spaces are not alowed, use a dash if you really need a separator.`"
)
UNVALID = (
    "How do i pronounce that?\n"
    "`Use only latin letters dash(-) and aphostrophe(').`"
)
SUCCESS = (
    "Ok! i'll call you {0} from now on."
)


class SetName(Feature):
    def __init__(self):
        self.permissions = {
            "pname": "set_name",
            "default": True,
            "bot_min": 3,
            "server_min": 3,
            "enabler": [],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_):
        return True

    def is_valid(self, *, client, message, data, cache, **_):
        if client.user == message.author:
            return False
        match = CHECK.search(message.content)
        if match is not None:
            cache.append(match)
            return True
        return False

    async def on_message(self, *, client, message, data, cache, **_):
        match = cache.pop()
        name = match.group(1)
        if len(name) > 12:
            mess = TOO_LONG
        elif len(name) < 2:
            mess = TOO_SHORT
        elif " " in name:
            mess = NO_FULL
        elif TEST_UNVALID.search(name) is not None:
            mess = UNVALID
        else:
            mess = SUCCESS.format(name)
            if message.guild is not None:
                data["member"]["name"] = name
            else:
                data["user"]["name"] = name

        if message.guild is not None and data["channel"]["type"] == "roleplay":
            mess = f"({mess})"
        await message.channel.send(mess)
