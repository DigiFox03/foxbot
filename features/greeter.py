import logging
from foxcore.feature import Feature

log = logging.getLogger("foxbot." + __name__)


class Greeter(Feature):
    def is_worth(self, *, client, message, data, **_):
        if message.guild is not None:
            permissions = message.guild.me.permissions_in(channel=message.channel)
            return permissions.send_messages
        return True

    def is_valid(
            self, *, client, message, data,
            rel_user_act, rel_memb_act=None, **_
    ):
        if message.author.bot:
            return False
        if message.guild is not None and data["member"]["to_greet"]:
            return True
        if message.guild is None and data["user"]["to_greet"]:
            return True
        return False

    def is_permitted(self, **_):
        return True

    async def on_message(self, *, client, message, data, **_):
        if message.guild is None:
            name = data["user"]["name"]
            data["user"]["to_greet"] = False
            new = not data["user"]["first_greet"]
            data["user"]["first_greet"] = True
        else:
            name = data["member"]["name"]
            data["member"]["to_greet"] = False
            new = not data["member"]["first_greet"]
            data["member"]["first_greet"] = True
        if not new:
            mess = f"Hi {name}"
        else:
            mess = f"Hi {name}\n`if that is not your name tell it to me with:\n my name is <name>`"
        if message.guild is not None and data["channel"]["type"] == "roleplay":
            mess = f"({mess})"
        await message.channel.send(mess)
