import re
from collections import Iterable
import yaml
from foxcore import BaseFeature


class UnitHelper(BaseFeature):
    def __init__(self, setting_file):
        with open(setting_file) as file:
            data = yaml.load(file)
        self.converters = [_Converter(**datap) for datap in data]
        self.permissions = {
            "pname": "units",
            "default": True,
            "bot_min": 3,
            "server_min": 3,
            "enabler": [],
            "disabler": []
        }

    def is_worth(self, message, **_):
        if message.guild is not None:
            permissions = message.guild.me.permissions_in(channel=message.channel)
            return permissions.send_messages
        return True

    def is_valid(self, client, message, cache, **_):
        if client.user == message.author:
            return False
        res = [c.conv(message.content) for c in self.converters]
        for sec in res:
            if sec:
                cache.append(res)
                return True
        return False

    async def on_message(self, message, cache, data, **_):
        res = cache.pop()
        ms = []
        for sec, cv in zip(res, self.converters):
            ms += [cv.repres(elm) for elm in sec if elm]
        mess = "\n".join(ms)
        if message.guild is not None and data["channel"]["type"] == "roleplay":
            mess = f"({mess})"
        await message.channel.send(mess)


class _Converter(object):
    def __init__(self, name, inputs, mul, rep):
        self.name = self.__name__ = name
        self.inputs = [re.compile(p, re.IGNORECASE) for p in inputs]
        self.mul = [[m] if not isinstance(m, Iterable) else m for m in mul]
        self.rep = rep

    def conv(self, string):
        res = [rg.findall(string) for rg in self.inputs]
        res = [[r] if not isinstance(r, Iterable) else r for r in res]
        res = [[float(r.replace(",", ".")) for r in ra] for ra in res]
        values = []
        for sec, mul in zip(res, self.mul):
            if sec:
                values.append(sum(v * c for v, c in zip(sec, mul)))
        return values

    def repres(self, value):
        values = [self._rep_sub(value, **elm) for elm in self.rep]
        return " = ".join([v for v in values if v is not None])

    @classmethod
    def _rep_sub(cls, value, div, form):
        value /= div
        for elm in form:
            if value < elm["min"]:
                r = cls._rep_run(value, **elm)
                if r:
                    return r
        return None

    @staticmethod
    def _rep_run(value, div, rep, skip=0, **_):
        values = []
        if not isinstance(div, (list, tuple)):
            div = [div]
        for elm in div:
            v = int(value / elm)
            value -= v * elm
            values.append(v)
        values[-1] += value / elm
        if values[-1] < skip:
            return None
        return rep.format(*values)
