import re
import random
import logging
import yaml
from foxcore.feature import Feature
log = logging.getLogger("foxbot." + __name__)


class Interactions(Feature):
    def __init__(self, triggersfile):
        fmt = {
            "botname": "fox-bot",
            "botnamereg": r"(?:\bfox-?bot\b|\bfox\b)"
        }
        try:
            with open(triggersfile) as file:
                log.info("compiling regex from %s", triggersfile)
                data = yaml.load(file)
                interactions = []
                for inter in data:
                    log.debug("compiling interaction %s", inter["name"])
                    interactions.append(
                        _Interaction(fmt, **inter)
                    )
                if not data:
                    log.warning("interactions file is empty")
                self.interactions = interactions
        except yaml.error.YAMLError:
            log.error("error while compiling", exc_info=1)
        self.permissions = {
            "pname": "interactions",
            "default": True,
            "bot_min": 3,
            "server_min": 3,
            "enabler": [],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_):
        if message.guild is not None:
            permissions = message.guild.me.permissions_in(channel=message.channel)
            return permissions.send_messages
        return True

    def is_valid(self, *, client, message, data, cache, **_):
        if message.author == client.user:
            return False
        for inter in self.interactions:
            resp = inter.get_response(message.content)
            if resp is not None:
                cache.append(resp)
                return True
        return False

    def is_permitted(self, *, client, message, data, **_):
        return True

    async def on_message(self, *, client, message, data, cache, **_):
        if message.guild is not None:
            name = data["member"]["name"]
        else:
            name = data["user"]["name"]
        resp = cache.pop().format(
            username=message.author.display_name,
            name=name,
            botname="fox-bot"
        )
        if message.guild is not None and data["channel"]["type"] == "roleplay":
            resp = f"({resp})"
        await message.channel.send(resp)


class _Interaction(object):
    def __init__(
            self, fmt, *,
            name, regex, responses, chances=1, map_response=False
    ):
        self.name = name
        self.mapped = map_response
        if self.mapped:
            self.regex = [self._regex_compile(reg, fmt) for reg in regex]
            self.responses = [self._string_ready(s) for s in responses]
        else:
            self.regex = self._regex_compile(regex, fmt)
            self.responses = self._string_ready(responses)
        self.chances = chances

    def get_response(self, inp):
        if self.chances > 1 and random.random() * self.chances > 1:
            return None
        if self.mapped:
            for part, resp in zip(self.regex, self.responses):
                for reg in part:
                    if reg.search(inp) is not None:
                        return random.choice(resp)
        else:
            for reg in self.regex:
                if reg.search(inp) is not None:
                    return random.choice(self.responses)
        return None

    @staticmethod
    def _regex_compile(regex, fmt):
        if isinstance(regex, str):
            regex = [regex]
        cmp = []
        for reg in regex:
            try:
                reg = reg.format(**fmt)
                cmp.append(re.compile(reg, re.IGNORECASE))
            except (KeyError, re.error):
                log.warning("error while compiling %s", reg, exc_info=1)
        return cmp

    @staticmethod
    def _string_ready(strings):
        if isinstance(strings, str):
            strings = [strings]
        return strings
