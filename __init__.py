import logging
import logging.config
import yaml
import discord
import foxcore
import features
import ppsteps

with open("logging.yaml") as file:
    logging.config.dictConfig(yaml.load(file))
log = logging.getLogger("foxbot")
actlog = logging.getLogger("foxbot.activity")

try:
    with open("settings.yaml") as file:
        settings = yaml.load(file)
        DISCORD_TOKEN = settings["discordToken"]
        MONGOADDRESS = settings["mongoaddress"]
except Exception:
    log.critical("error while reading settings\n\n", exc_info=1)
    raise SystemExit

try:
    with open("permission_errors.yaml") as file:
        pset = yaml.load(file)
except Exception:
    log.critical("error while loading permissions errors file\n\n", exc_info=1)
    raise SystemExit

prefix = "f-"

discord_client = discord.Client()
mongo_client = foxcore.blocks.mongodb.ClientMongo(MONGOADDRESS)

mongo_fetch = foxcore.blocks.mongodb.Fetcher(mongo_client)
mongo_update = foxcore.blocks.mongodb.Updater(mongo_client)
mogno_default = foxcore.blocks.mongodb.Defaulter(mongo_client)
statistics = foxcore.blocks.statistics.Statistics(mongo_client)
activity = foxcore.blocks.activity.Activity()
channel_perm = foxcore.blocks.channel_perm.ChannelPerm(prefix)
counter = foxcore.blocks.usecounter.Counter(logger=actlog)

ppstep_list = [
    ppsteps.Greeter(4 * 3600)
]

feature_list = [
    foxcore.features.ChannelSetting(prefix),
    foxcore.features.Ping(prefix),
    foxcore.features.PipelineDebug(prefix),
    foxcore.features.StatsUpdate(statistics, prefix),
    features.UnitHelper("converter.yaml"),
    features.Interactions("triggers.yaml"),
    features.SetName(),
    features.Greeter(),
]

preprocess_handler = foxcore.blocks.preprocess.Preprocess(ppstep_list)
features_handler = foxcore.blocks.features.Handler(feature_list, pset)
pipeline = foxcore.pipeline.Pipeline()
pipeline.set_blocks([
    foxcore.blocks.usecounter.CounterBlock(counter, "enter"),
    mongo_fetch,
    mogno_default,
    foxcore.blocks.infoprinter.InfoPrinter(logging.getLogger("foxbot.info")),
    activity,
    channel_perm,
    preprocess_handler,
    features_handler,
    statistics,
    mongo_update,
    foxcore.blocks.usecounter.CounterBlock(counter, "exit"),
])


@discord_client.event
async def on_message(message):
    if message.guild is not None:
        log.info(
            "handling message from %s in %s in %s",
            message.author, message.channel, message.guild
        )
    else:
        log.info(
            "handling direct message from %s",
            message.author
        )
    await pipeline.run({
        "message": message,
        "client": discord_client
    })


@discord_client.event
async def on_ready():
    log.info("logged in")
    actlog.info("started session")
    mt = 0
    for guild in discord_client.guilds:
        for channel in guild.text_channels:
            try:
                async for message in channel.history(limit=30):
                    tm = discord.utils.snowflake_time(message.id).timestamp()
                    mt = max(tm, mt)
                    if tm - mt > 4 * 3600:
                        break
                    await activity.input(message=message)
            except discord.Forbidden:
                log.warning("could not read history in %s in %s", channel, guild)
    log.info("ready")

log.info("connecting")
discord_client.run(DISCORD_TOKEN)
actlog.info("ended session")
